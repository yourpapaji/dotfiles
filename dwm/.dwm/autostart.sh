#!/usr/bin/env bash

function run {
  if ! pgrep -f $1 ;
  then
    $@&
  fi
}

run xsetroot -cursor_name left_ptr &
run xfsettingsd &
run xfce4-power-manager &
run xautolock -time 10 -locker slocker &
run numlockx on &
run clipmenud &
run compton -b --config ~/.config/compton.conf &
run unclutter --timeout 2 &
#run redshift -l 30.73629:76.7884 &
run redshift &
run fehbg-random &
run mpd &
run urxvtd --quiet --opendisplay --fork &
exec /usr/libexec/polkit-gnome-authentication-agent-1 &
run slstatus &
