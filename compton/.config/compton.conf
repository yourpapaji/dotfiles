# Backend - [glx/xrender]
backend = "glx";
glx-no-stencil = true;
glx-copy-from-front = false;
use-ewmh-active-win = true;
unredir-if-possible = true;
glx-no-rebind-pixmap = true;
#glx-swap-method = "exchange";
xrender-sync-fence = true;
mark-wmwin-focused = true;
mark-ovredir-focused = true;
detect-transient = true;
detect-client-leader = true;
detect-rounded-corners = true;
refresh-rate = 60;
vsync = true;
dbe = false;

# Logs
log-level = "warn";
log-file = "~/.config/compton.log";

# Fading
fading = true;                  # Fade windows during opacity changes.
fade-delta = 8;                 # The time between steps in a fade in milliseconds. (default 10).
fade-in-step = 0.048;           # Opacity change between steps while fading in. (default 0.028).
fade-out-step = 0.16;           # Opacity change between steps while fading out. (default 0.03).
#no-fading-openclose = true;     # Fade windows in/out when opening/closing.

# Shadows
shadow = true;
shadow-ignore-shaped = true;
shadow-radius = 10;
shadow-offset-x = -10;
shadow-offset-y = -10;
shadow-opacity = 0.5;
shadow-exclude = [
    "class_g ?= 'Notify-osd'",
    "_NET_WM_STATE@:32a *= '_NET_WM_STATE_HIDDEN'",
    "_GTK_FRAME_EXTENTS@:c",
    "_NET_WM_OPAQUE_REGION@:c",
    "x = 0 && y = 0 && override_redirect = true",
    "_NET_WM_STATE@[0]:32a *= '_NET_WM_STATE_HIDDEN'",
    "_NET_WM_STATE@[1]:32a *= '_NET_WM_STATE_HIDDEN'",
    "_NET_WM_STATE@[2]:32a *= '_NET_WM_STATE_HIDDEN'",
    "_NET_WM_STATE@[3]:32a *= '_NET_WM_STATE_HIDDEN'",
    "_NET_WM_STATE@[4]:32a *= '_NET_WM_STATE_HIDDEN'",
    "_NET_WM_STATE@:32a *= '_NET_WM_STATE_HIDDEN'"
];

# Opacity
opacity-rule = [
    "0:_NET_WM_STATE@[0]:32a *= '_NET_WM_STATE_HIDDEN'",
    "0:_NET_WM_STATE@[1]:32a *= '_NET_WM_STATE_HIDDEN'",
    "0:_NET_WM_STATE@[2]:32a *= '_NET_WM_STATE_HIDDEN'",
    "0:_NET_WM_STATE@[3]:32a *= '_NET_WM_STATE_HIDDEN'",
    "0:_NET_WM_STATE@[4]:32a *= '_NET_WM_STATE_HIDDEN'",
    "0:_NET_WM_STATE@:32a *= '_NET_WM_STATE_HIDDEN'"
];

# Blur
blur-background = false;
#blur-background-fixed = true;
#blur-kern = "11x11gaussian";
#blur-strength = 12;
#blur-background-exclude = [
#    "name = 'tray'",
#    "class_g = 'Plank'",
#    "window_type = 'desktop'",
#    "window_type = 'tooltip'",
#    "_GTK_FRAME_EXTENTS@:c"
#];

# Focus
focus-exclude = [
    "x = 0 && y = 0 && override_redirect = true",
    "_NET_WM_STATE@:32a *= '_NET_WM_STATE_HIDDEN'",
    "_GTK_FRAME_EXTENTS@:c"
];

# Other Options
wintypes:
{
    tooltip = { fade = true; shadow = true; opacity = 0.9; focus = true;};
    dock = { shadow = true; blur-background = true; }
    dnd = { shadow = false; blur-background = false; }
    popup_menu = { opacity = 0.9; shadow = false; }
    menu = { opacity = 0.9; shadow = true; }
    dropdown_menu = { opacity = 0.9; shadow = false; }
    utility = { shadow = false; blur-background = true; }
};
