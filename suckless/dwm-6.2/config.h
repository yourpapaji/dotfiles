/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int gappx     = 16;       /* gaps between windows */
static const unsigned int snap      = 0;        /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "Iosevka Nerd Font:size=10" };
static const char dmenufont[]       = "Iosevka Nerd Font:size=10";
static const char col_gray1[]       = "#1d2021";
static const char col_gray2[]       = "#458588";
static const char col_gray3[]       = "#928374";
static const char col_gray4[]       = "#d3869b";
static const char col_gray5[]       = "#458588";
static const char col_cyan[]        = "#32302f";
static const char col_font[]        = "#ebdbb2";
static const unsigned int baralpha = 255;
static const unsigned int borderalpha = OPAQUE;
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_font, col_gray1, col_gray3 },
	[SchemeSel]  = { col_gray4, col_cyan,  col_gray5  },
};
static const unsigned int alphas[][3]      = {
	/*               fg      bg        border     */
	[SchemeNorm] = { OPAQUE, baralpha, borderalpha },
	[SchemeSel]  = { OPAQUE, baralpha, borderalpha },
};

/* tagging */
static const char *tags[] = { "", "", "", "", "", "", "", "", "" };

static const Rule rules[] = {
	/* class                                    instance    title     tags mask   iscentered  isfloating   monitor */
	{ NULL,                                     NULL,       NULL,     0,          1,          0,           -1 },
	{ "Gimp",                                   NULL,       NULL,     1 << 6,     1,          1,           -1 },
	{ "mpv",                                    NULL,       NULL,     0,          1,          1,           -1 },
	{ "Thunar",                                 NULL,       NULL,     0,          1,          1,           -1 },
	{ "Kvantum Manager",                        NULL,       NULL,     0,          1,          1,           -1 },
	{ "Sxiv",                                   NULL,       NULL,     0,          1,          1,           -1 },
	{ "libreoffice-startcenter",                NULL,       NULL,     1 << 6,     1,          1,           -1 },
	{ "Meld",                                   NULL,       NULL,     0,          1,          1,           -1 },
	{ "Catfish",                                NULL,       NULL,     0,          1,          1,           -1 },
	{ "Gnome-disks",                            NULL,       NULL,     0,          1,          1,           -1 },
	{ "Clamtk",                                 NULL,       NULL,     0,          1,          1,           -1 },
	{ "qBittorrent",                            NULL,       NULL,     1 << 7,     1,          1,           -1 },
	{ "Notepadqq",                              NULL,       NULL,     0,          1,          1,           -1 },
	{ "discord",                                NULL,       NULL,     1 << 7,     1,          1,           -1 },
	{ "Nm-connection-editor",                   NULL,       NULL,     0,          1,          1,           -1 },
	{ "Lutris",                                 NULL,       NULL,     1 << 4,     1,          1,           -1 },
	{ "Steam",                                  NULL,       NULL,     1 << 4,     1,          1,           -1 },
	{ "Pavucontrol",                            NULL,       NULL,     0,          1,          1,           -1 },
	{ "Nvidia-settings",                        NULL,       NULL,     0,          1,          1,           -1 },
	{ "VirtualBox Manager",                     NULL,       NULL,     1 << 6,     1,          1,           -1 },
	{ "Xfce4-about",                            NULL,       NULL,     0,          1,          1,           -1 },
	{ "Chromium-browser-ungoogled-chromium",    NULL,       NULL,     1 << 8,     0,          0,           -1 },
	{ "Vivaldi-snapshot",                       NULL,       NULL,     1 << 3,     0,          0,           -1 },
	{ "xterm-256color",                         NULL,       NULL,     0,          1,          0,           -1 },
	{ "xterm-256color",                         NULL,       "htop",   0,          1,          1,           -1 },
	{ "xterm-256color",                         NULL,       "ncmpcpp", 0,          1,          1,           -1 },
	{ "xterm-256color",                         NULL,       "ranger", 0,          1,          1,           -1 },
	{ "Nightly",                                NULL,       NULL,     1 << 1,     1,          0,           -1 },
	{ "Nightly",                                NULL,  "Library",      0,         1,          1,           -1 },
	{ "Nightly",                                NULL, "About Nightly", 0,         1,          1,           -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define ALTKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *termcmd2[]  = { "urxvtc", NULL };
static const char *browser[]  = { "firefox", NULL };
static const char *programs[] = { "j4-dmenu-desktop", NULL };

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ ALTKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_p,      spawn,          {.v = programs } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY|ControlMask,           XK_Return, spawn,          {.v = termcmd2 } },
	{ MODKEY,                       XK_w,      spawn,          {.v = browser } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,                       XK_q,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY,                       XK_minus,  setgaps,        {.i = -1 } },
	{ MODKEY,                       XK_equal,  setgaps,        {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = 0  } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
	{ ALTKEY,                       XK_Up,     spawn,          SHCMD("pulseaudio-ctl up 5") },
	{ ALTKEY,                       XK_Down,   spawn,          SHCMD("pulseaudio-ctl down 5") },
	{ ALTKEY,                       XK_m,      spawn,          SHCMD("pulseaudio-ctl mute") },
	{ ALTKEY,                       XK_l,      spawn,          SHCMD("slocker") },
	{ ALTKEY,                       XK_c,      spawn,          SHCMD("clipmenu") },
	{ MODKEY|ALTKEY,                XK_c,      spawn,          SHCMD("compton-toggle") },
	{ ALTKEY,                       XK_u,      spawn,          SHCMD("emojimenu") },
	{ ALTKEY,                       XK_n,      spawn,          SHCMD("networkmanager_dmenu") },
	{ MODKEY|ControlMask,           XK_p,      spawn,          SHCMD("st -e htop") },
	{ MODKEY|ControlMask,           XK_m,      spawn,          SHCMD("st -e ncmpcpp") },
	{ MODKEY|ControlMask,           XK_i,      spawn,          SHCMD("st -e weechat") },
	{ MODKEY|ControlMask,           XK_f,      spawn,          SHCMD("st -g=150x40 -e ranger") },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

