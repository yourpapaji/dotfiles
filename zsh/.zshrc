# If you come from bash you might have to change your $PATH.

# Path to your oh-my-zsh installation.
export ZSH="/home/k4rm4n/.oh-my-zsh"

#ZSH_THEME="gentoo"
#ZSH_THEME="crunch"
ZSH_THEME="afowler"
export UPDATE_ZSH_DAYS=3
ENABLE_CORRECTION="true"
COMPLETION_WAITING_DOTS="true"

plugins=(git colorize zsh-syntax-highlighting zsh-autosuggestions history-substring-search sudo)

source $ZSH/oh-my-zsh.sh

# environment
export LANG=en_US.UTF-8
HISTFILE=~/.cache/zsh/.zhistory
HISTSIZE=10000
SAVEHIST=10000
export EDITOR=/usr/bin/nvim
export VISUAL=/usr/bin/nvim

# Completion
autoload -U compinit
compinit

## Keybindings section
bindkey -e
bindkey '^[[7~' beginning-of-line                               # Home key
bindkey '^[[H' beginning-of-line                                # Home key
if [[ "${terminfo[khome]}" != "" ]]; then
    bindkey "${terminfo[khome]}" beginning-of-line              # [Home] - Go to beginning of line
fi
bindkey '^[[8~' end-of-line                                     # End key
bindkey '^[[F' end-of-line                                      # End key
if [[ "${terminfo[kend]}" != "" ]]; then
    bindkey "${terminfo[kend]}" end-of-line                     # [End] - Go to end of line
fi
bindkey '^[[2~' overwrite-mode                                  # Insert key
bindkey '^[[3~' delete-char                                     # Delete key
bindkey '^[[C'  forward-char                                    # Right key
bindkey '^[[D'  backward-char                                   # Left key
bindkey '^[[5~' history-beginning-search-backward               # Page up key
bindkey '^[[6~' history-beginning-search-forward                # Page down key

# Navigate words with ctrl+arrow keys
bindkey '^[Oc' forward-word                                     #
bindkey '^[Od' backward-word                                    #
bindkey '^[[1;5D' backward-word                                 #
bindkey '^[[1;5C' forward-word                                  #
bindkey '^H' backward-kill-word                                 # delete previous word with ctrl+backspace
bindkey '^[[Z' undo                                             # Shift+tab undo last action

# alias
alias vim="nvim"
alias gitu='git add . && git commit && git push'
alias emerge='sudo emerge'
alias ls='exa --color=always --group-directories-first'
alias la='exa -a --color=always --group-directories-first'
alias ll='exa -l --color=always --group-directories-first'
alias lla='exa -al --color=always --group-directories-first'
alias sX='ls | grep'
alias sXa='ls -a | grep'
alias vpnC='sudo protonvpn-cli --connect'
alias vpnD='sudo protonvpn-cli --disconnect'
alias hibernate='sudo /usr/sbin/pm-hibernate'
alias visudo='sudo EDITOR=nvim visudo'
